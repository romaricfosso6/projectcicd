 

var Packages = {
    nodes: [
                                                                                                        
                
{
    "id": "tn.esprit.com",
    "text": "tn.esprit.com",
    "package": "tn.esprit.com",
    "url": "tn/esprit/com/pkg-summary.html",
            "coverage": "88,2%",
        "icon": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed",
            "li_attr": {"data-is-link": "true"},
        "a_attr": {"href": "tn/esprit/com/pkg-summary.html"},
    "children": [
                                            
                
{
    "id": "tn.esprit.com.test",
    "text": "test",
    "package": "tn.esprit.com.test",
    "url": "tn/esprit/com/test/testsrc-pkg-summary.html",
            "coverage": "100%",
        "icon": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed",
            "li_attr": {"data-is-link": "true"},
        "a_attr": {"href": "tn/esprit/com/test/testsrc-pkg-summary.html"},
    "children": [
                    ]
},
            ]
},
            ],
    settings: {
        "icons": {
            "package": {
                "open": "aui-icon aui-icon-small aui-iconfont-devtools-folder-open",
                "closed": "aui-icon aui-icon-small aui-iconfont-devtools-folder-closed"
            },
            "state": {
                "collapsed": "aui-icon aui-icon-small aui-iconfont-collapsed",
                "expanded": "aui-icon aui-icon-small aui-iconfont-expanded",
                "forRemoval": "hidden aui-iconfont-collapsed aui-iconfont-expanded"
            }
        }
    }
};
